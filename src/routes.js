import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Teste = React.lazy(() => import('./views/teste/Teste'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/teste', name: 'Teste', component: Teste },

];

export default routes;
