import React, { Suspense } from 'react'
import {
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import { Container, CircularProgress } from '@material-ui/core/';
import styles from './styles.module.scss';

import routes from '../routes'

const loading = (<CircularProgress color="secondary" />)

const TheContent = () => {
  return (
    <main className={styles.c_main}>
      <Container>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return route.component && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={props => (
                    <div>
                      <route.component {...props} />
                    </div>
                  )}
                />
              )
            })}
            <Redirect from="/" to="/dashboard" />
          </Switch>
        </Suspense>
      </Container>
    </main>
  )
}

export default TheContent
