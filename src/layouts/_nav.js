import React from 'react'
import AccountBalanceOutlinedIcon from '@material-ui/icons/AccountBalanceOutlined';

const _nav = [
  {
    _tag: '',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <AccountBalanceOutlinedIcon/>
  },
  {
    _tag: '',
    _children: ['Tema Teste']
  },
  {
    _tag: '',
    name: 'Teste',
    to: '/teste',
    icon: <AccountBalanceOutlinedIcon/>
  }
]

export default _nav
